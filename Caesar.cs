﻿using System;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Net.Mime.MediaTypeNames;

namespace CaesarCipherApp
{
    internal class CaesarCipher
    {
        public Dictionary<char, char> CreateAlphabet(int key, string keyword)
        {
            string englishAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz!@#$%^&*()_+[]{}|;':,.<>?";
            string changedAlphabet = englishAlphabet.Substring(englishAlphabet.Length - key, key) + keyword;
            foreach (char letter in englishAlphabet)
            {
                if (!changedAlphabet.Contains(letter))
                {
                    changedAlphabet += letter.ToString();
                }
            }
            Dictionary<char, char> cipher = englishAlphabet.Zip(changedAlphabet, (c1, c2) => new KeyValuePair<char, char>(c1, c2))
                                                      .ToDictionary(pair => pair.Key, pair => pair.Value);
            return cipher;
        }
        public string Cipher(string filepath, int key, string keyword)
        {
            string text = File.ReadAllText(filepath);
            Dictionary<char, char> cipherAlphabet = CreateAlphabet(key, keyword);
            StringBuilder stringBuilder = new StringBuilder();
            foreach (char c in text)
            {
                stringBuilder.Append(cipherAlphabet[c]);
            }
            Console.WriteLine("Шифрування: " + stringBuilder.ToString());
            return stringBuilder.ToString();
        }

        public string Decipher(string str, int key, string keyword)
        {
            Dictionary<char, char> cipherAlphabet = CreateAlphabet(key, keyword);
            StringBuilder stringBuilder = new StringBuilder();
            foreach (char c in str)
            {
                stringBuilder.Append(cipherAlphabet.FirstOrDefault(x => x.Value == c).Key);
            }
            Console.WriteLine("Дешифрування: " + stringBuilder.ToString());
            return stringBuilder.ToString();
        }
    }
}
