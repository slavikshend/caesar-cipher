﻿namespace CaesarCipherApp
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Input key: ");
            int key = int.Parse(Console.ReadLine());
            Console.Write("Input keyword: ");
            string keyword = Console.ReadLine();
            if (keyword.Length > 25 || keyword.Distinct().Count() != keyword.Length)
            {
                Console.WriteLine("Error");
                Console.ReadKey();
                Environment.Exit(0);
            }
            Console.Write("Enter file path: ");
            string path = Console.ReadLine();
            CaesarCipher caesar = new CaesarCipher();
            string str = caesar.Cipher(path, key, keyword);
            caesar.Decipher(str, key, keyword);
            Console.ReadKey();
        }
    }
}
